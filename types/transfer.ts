import {TransferItem} from "./TransferItem";

export enum TransferStatus {
    OPEN = "open",
    STARTED = "started",
    CLOSED = "closed",
}

export default interface Transfer {
    id: number,
    status: TransferStatus,
    created_at: number,
    putaway_id:number,
    items: TransferItem[]
}
