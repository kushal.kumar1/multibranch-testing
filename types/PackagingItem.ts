
export interface PackagingItemType {
    data : PackagingItemData[];
    meta : PackagingItemMeta;
}

export interface PackagingItemData {
    id: number
    order_id: string
    total_items: number
}

interface PackagingItemMeta {
    current_page: number,
    total: number
}

