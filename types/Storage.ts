export default interface Storage {
    id: number,
    label: string,
    category: string,
    type: string
}
