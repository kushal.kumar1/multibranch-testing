import * as yup from "yup";
import Product from "./Product";

export enum AuditItemStatus {
  PENDING_ACTION = "pending_action",
  PENDING_APPROVAL = "pending_approval",
  COMPLETED = "completed",
  CANCELLED="cancelled",
  ASSIGNED="assigned"
}

export interface AuditItem {
  new_storage_label?: AuditItem;
  id: number;
  auditId : number,
  product: Product,
  storage: Storage,
  systemQuantity: number,
  status : AuditItemStatus
  
}