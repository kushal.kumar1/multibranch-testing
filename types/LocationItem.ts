import * as yup from "yup";
import { AuditItem } from "./AuditItem";

export enum LocationItemStatus {
  PENDING = "pending",
  PROCESSING = "processing",
  COMPLETED = "completed",
}

export interface LocationItem {
  id: number;
  name : string,
  type:string,
  status: LocationItemStatus;
  items: AuditItem[]
   
}