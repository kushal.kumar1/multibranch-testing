const withPWA = require("next-pwa");
const nextConfig = {
    useFileSystemPublicRoutes: true,
    // webpack5: false,
    pwa: {
        dest: 'public',
        disable: process.env.NODE_ENV === 'development',
        // dynamicStartUrlRedirect: '/login'
    },
    
    webpack: config => {
        config.module.rules.push({
          test: /\.bin$/i,
          type: 'asset/resource',
          generator: {
            // important, otherwise it will output into a folder that is not served by next
            filename: 'static/[hash][ext][query]'
          },
        });
        config.resolve.fallback = {
            ...config.resolve.fallback, // if you miss it, all the other options in fallback, specified
              // by next.js will be dropped. Doesn't make much sense, but how it is
            fs: false, // the solution
          };
        return config
      },
    
};

const dev = process.env.NODE_ENV !== 'production';
module.exports = dev ? nextConfig : withPWA(nextConfig);
