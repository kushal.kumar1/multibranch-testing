import Modal from "../../../layouts/Modal";
import React, {FormEvent, useState} from 'react';
import AsyncSelect from 'react-select/async';
import PutawayApi from "../../../apis/PutawayApi";
import {useSelector} from "react-redux";
import {RootState} from "../../../redux/store";
import Toolkit from "../../../utils/Toolkit";

const AssignStorageModal = ({complete, item, assignStorage}) => {
    const [selectedStorage, setSelectedStorage] = useState(null);
    const [quantity, setQuantity] = useState<number>(item.quantity);

    const handleChange = (value) => {
        setSelectedStorage(value.id);
    }

    const rootState = useSelector((state: RootState) => state);
    const apiProps = Toolkit.mapStateToApiProps(rootState);

    const loadStorages = async (value) => {
        const response = await new PutawayApi(apiProps).searchStorage(value);
        return response.data.data;
    };
    
    const submit = async (e: FormEvent) => {
        e.preventDefault();

        if(selectedStorage) {
            const params = {
                putaway_item_id: item.id, 
                destination_storage_id: selectedStorage,
                placed_quantity: quantity,
                variant : item.sku.substring(item.sku.indexOf('-') + 1)
            };
            assignStorage(params);
            complete();
        }
        
    }

    return (
        <Modal>
            <h1 className="title mt-lg">{item.product.name}</h1>
            <p>Quantity: {item.quantity} ({item.sku.substring(item.sku.indexOf('-') + 1)})</p>
            <p>Barcode: {item.product.barcode}</p>
            <form className="mt-lg" onSubmit={submit}>
                <div>
                    <label className="label">Assign to Storage</label>
                    <AsyncSelect
                        autoFocus
                        getOptionLabel={e => e.label}
                        getOptionValue={e => e.id}
                        loadOptions={loadStorages}
                        onChange={handleChange}
                    />
                </div>
                <div>
                    <label className="label">Quantity</label>
                    <input
                        value={quantity}
                        max={item.quantity}
                        min={0}
                        onChange={(e) => {
                        setQuantity(
                            parseInt(e.target.value) === NaN ? 0 : parseInt(e.target.value)
                        );
                        }}
                        className="input input-sm"
                        type="number"
                    />
                </div>
                <div className="mt-lg center">
                    <button className="btn btn-green">ASSIGN</button>
                </div>
            </form>
        </Modal>
    );
};

export default AssignStorageModal;
