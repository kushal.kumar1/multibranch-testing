import moment from "moment";
import AppAction from "../../redux/actions/AppAction";
import PutawayAction from "../../redux/actions/PutawayAction";
import { AppActionTypes } from "../../redux/ActionTypes";
import { useThunkyDispatch } from "../../redux/store";
import { PutawayItem } from "../../types/PutawayItem";
import { ModalName } from "../ModalManager";

const ListItem = ({setResults,item}) => {
    const dispatch = useThunkyDispatch();
    const openAddToCartModal = async (item) => {
        dispatch(AppAction.switchModal(ModalName.ADD_PRODUCT, {onSubmit: addToCart, item: item}));
        
    }

    const addToCart = async (quantity) => {
        const apiParams = {
            product_id :item.product.id,
            variant :item.sku.substring(item.sku.indexOf('-') + 1),
            quantity :quantity,
            migration_item_id:item.id,
            sku: item.sku
        }
        const addedtoCart = await dispatch(PutawayAction.addToCart(apiParams));
        
        if(addedtoCart) {

            setResults(results=>results.filter(result=>result.id != addedtoCart.id))
            dispatch({
                type: AppActionTypes.DISPLAY_MESSAGE,
                payload: {text: 'Successfully added item to cart', isError: false, milliseconds: 1000}
            });
        }
    }

    return (
            <div className="card p-md ripple">
                <div className="grid gutter-between no-select">
                <div className="col-10">
                        <div className="semi-bold">{item.product.name}</div>
                        <div className="mt-xs xs">Quantity: {item.quantity - item.placed_quantity}</div>
                        
                    </div>
                    {
                        <div className="col-2">
                            <div onClick={()=>openAddToCartModal(item)} style={{border: "none"}} className={`orange center mt-xs`}>
                                <i style={{fontSize: "x-large"}} className="fas fa-cart-plus"></i><br/>
                                <div className="xs">Add to<br/>Cart</div>
                            </div>
                        </div>
                    }
                </div>
            </div>
    );
}

export default ListItem;