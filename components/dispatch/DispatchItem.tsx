import router from "next/router";
import AppAction from "../../redux/actions/AppAction";
import { useThunkyDispatch } from "../../redux/store";
import { PackagingItemType } from "../../types/PackagingItem";
import Link from "../Link";
import { ModalName } from "../ModalManager";

const DispatchItem = ({ item }) => {
    const dispatch = useThunkyDispatch();
    const openScanModal = async () => {
        // dispatch(AppAction.switchModal(ModalName.VERIFY_STORAGE, {onSubmit:()=>{
        //     console.log("submitted");
        // }, item: item}));
        dispatch(
            AppAction.switchModal(ModalName.DISPATCH_LOCATION_SCAN, {
                // productModal: false,
                action: handleStorageScan,
                storageLabelFromApi: item.label,
            })
        );
    };

    const handleStorageScan = () => {
        router.push({
            pathname: `/dispatch/${item.id}`,
            query: { id: item.id, label: item.label },
        });
    };

    return (
        <div onClick={openScanModal} className="col-12">
            <div className="card p-md sm">
                <div className="grid gutter-sm">
                    <div className="col-10">
                        <div className="semi-bold lg">{item.label}</div>
                    </div>

                    <div className="col-2">
                        <div
                            style={{ border: "none" }}
                            className="blue center mt-xs"
                            onClick={() => {}}
                        >
                            <i
                                style={{ fontSize: "x-large" }}
                                className="fas fa-qrcode"
                            ></i>
                            <br />
                            <div className="xs">Scan</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        // </Link>
    );
};

export default DispatchItem;
