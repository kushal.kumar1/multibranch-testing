import { PackagingItemType } from "../../types/PackagingItem";
import Link from "../Link";

interface propType {
    item: PackagingItemType;
}

const OrderItem = ({ items, handleMarkItem }) => {
    const handleOnClick = (item) => {
        //call post api and then  get api
        // console.log("api called");
        handleMarkItem(item.id);
    };
    return (
        <>
            {items &&
                items.map((item, index) => (
                    <div className="col-12" key={index}>
                        <div className="card p-md sm">
                            <div className="grid gutter-sm">
                                <div className="col-10">
                                    <div className="semi-bold lg">
                                        {item?.order_id}
                                    </div>
                                    <div className="mt-xs md">{item.id}</div>
                                </div>
                                {item.status === "manifested" ? (
                                    <div className="col-2">
                                        <div
                                            style={{ border: "none" }}
                                            className="blue center mt-xs"
                                            onClick={() => handleOnClick(item)}
                                        >
                                            <i
                                                style={{ fontSize: "x-large" }}
                                                className="fas fa-check"
                                            ></i>
                                            <br />
                                            <div className="xs">
                                                Mark
                                                <br />
                                                Picked
                                            </div>
                                        </div>
                                    </div>
                                ) : (
                                    <div className="col-2">
                                        <div
                                            style={{ border: "none" }}
                                            className="green center mt-xs"
                                        >
                                            <i
                                                className="fas fa-check-circle"
                                                style={{ fontSize: "x-large" }}
                                            ></i>
                                            <br />
                                            <div className="xs">
                                                Mark
                                                <br />
                                                Picked
                                            </div>
                                        </div>
                                    </div>
                                )}
                            </div>
                        </div>
                    </div>
                ))}
        </>
    );
};

export default OrderItem;

{
    /* <i class="fa-solid fa-circle-check"></i> */
}
