import React from "react";
import styles from "./Dispatch.module.css";

const cleanPercentage = (percentage) => {
    const tooLow = !Number.isFinite(+percentage) || percentage <= 0;
    const tooHigh = percentage > 100;
    return tooLow ? 0 : tooHigh ? 100 : +percentage;
};

const Circle: any = ({ width = 1.1, colour, pct }) => {
    const r = 80;
    const circ = 2 * Math.PI * r;
    const strokePct = ((100 - pct) * circ) / 100;

    return (
        <circle
            r={r}
            cx={100}
            cy={100}
            fill="transparent"
            stroke={ Math.floor(strokePct) !== Math.floor(circ) ? colour : ""} // remove colour as 0% sets full circumference
            strokeWidth={`${width}rem`}
            strokeDasharray={circ}
            strokeDashoffset={pct ? strokePct : 0}
            strokeLinecap="round"
        ></circle>
    );
};

const Text = ({ percentage, totalItems, completed }) => {
    return (
        <>
            <text
                className={styles.pieLargeText}
                x="50%"
                y="40%"
                dominantBaseline="central"
                textAnchor="middle"
            >
                {completed}/{totalItems}
            </text>
            <text
                x="50%"
                y="60%"
                dominantBaseline="central"
                textAnchor="middle"
                fontSize={"1.5em"}
            >
                Boxes
            </text>
            <text
                x="50%"
                y="70%"
                dominantBaseline="central"
                textAnchor="middle"
                fontSize={"1.5em"}
            >
                Loaded
            </text>
        </>
    );
};

const Pie = ({ colour, totalItems, completed }) => {
    const pct = cleanPercentage((completed / totalItems) * 100);
    return (
        <svg
            style={{ width: "200px", height: "200px" }}
            width={200}
            height={200}
        >
            <g transform={`rotate(-90 ${"100 100"})`}>
                <Circle colour="lightgrey" />
                <Circle width={0.7} colour={colour} pct={pct} />
            </g>
            <Text
                percentage={pct}
                totalItems={totalItems}
                completed={completed}
            />
        </svg>
    );
};

export default Pie;
