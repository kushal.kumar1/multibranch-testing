import React from "react";
import { usePagination, DOTS } from "../hooks/usePagination";
import styles from "./pagination.module.css";
const Pagination = (props: any) => {
  const { onPageChange, totalCount, siblingCount = 1, currentPage, pageSize } = props;

  const paginationRange = usePagination({
    currentPage,
    totalCount,
    siblingCount,
    pageSize,
  });

  // If there are less than 2 times in pagination range we shall not render the component
  if (currentPage === 0 || paginationRange.length < 2) {
    return null;
  }

  const onNext = () => {
    onPageChange(currentPage + 1);
  };

  const onPrevious = () => {
    onPageChange(currentPage - 1);
  };

  let lastPage = paginationRange[paginationRange.length - 1];
  return (
    <ul className={styles.paginationContainer}>
      {/* Left navigation arrow */}
      <li
        className={`${styles.paginationItem} ${currentPage === 1 ? styles.disabled : ""}`}
        onClick={() => {
          currentPage === 1 ? null : onPrevious();
        }}
      >
        <div className={`${styles.arrow} ${styles.left}`} />
      </li>
      {paginationRange.map((pageNumber, index) => {
        // If the pageItem is a DOT, render the DOTS unicode character
        if (pageNumber === DOTS) {
          return (
            <li key={`${pageNumber}+${index}`} className={styles.dots}>
              &#8230;
            </li>
          );
        }
        // Render our Page Pills
        return (
          <li
            className={`${styles.paginationItem} ${
              pageNumber === currentPage ? styles.selected : ""
            }`}
            onClick={() => onPageChange(pageNumber)}
            key={`${pageNumber}+${index}`}
          >
            {pageNumber}
          </li>
        );
      })}
      {/*  Right Navigation arrow */}
      <li
        className={`${styles.paginationItem} ${currentPage === lastPage ? styles.disabled : ""}`}
        onClick={() => {
          currentPage === lastPage ? null : onNext();
        }}
      >
        <div className={`${styles.arrow} ${styles.right}`} />
      </li>
    </ul>
  );
};

export default Pagination;
