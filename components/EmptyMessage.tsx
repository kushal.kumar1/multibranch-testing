interface Props {
    message: string,
    children?: any
}

const EmptyMessage = ({children, message}: Props) => {
    return (
        <div className="page-center center semi-bold">
            <img src="/images/empty.png"/>
            <div className="mt-md">{message}</div>
            <div>{children}</div>
        </div>
    );
};

export default EmptyMessage;