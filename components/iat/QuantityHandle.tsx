import style from "./quantityHandle.module.css";

const QuantityHandle = ({
  openScanModal,
  openNoQuantityFoundModal,
  productData,
}) => {
  return (
      <>
          <div
              className={`${style.foundProduct} mt-lg p-md`}
              onClick={() => openScanModal("location")}
          >
              <div className={style.left}>
                  <div className="bold md">Found the Product</div>
                  <div className="sm">
                      Scan the barcode using your camera to verify
                  </div>
              </div>

              <div className={style.right}>
                  <img src="/images/scan.svg" alt="scan" />
                  <div className="xs">Scan</div>
              </div>
          </div>
          <div
              className={`${style.notFoundProduct} mt-lg p-sm`}
              onClick={openNoQuantityFoundModal}
          >
              <div className={style.left}>
                  {productData.length === 0 ? (
                      <>
                          <div className="bold md">
                              Did not find the Product
                          </div>
                          <div className="sm">Click here to mark PNA</div>
                      </>
                  ) : (
                      <>
                          <div className="bold md">
                              Skip the rest of the quantity
                          </div>
                          <div className="sm">
                              Click here to mark rest of the quantity as PNA
                          </div>
                      </>
                  )}
              </div>
              <div className={style.right}>
                  <img src="/images/notFound.svg" alt="notFound" />
                  <div className="xs">Mark PNA</div>
              </div>
          </div>
      </>
  );
};

export default QuantityHandle;