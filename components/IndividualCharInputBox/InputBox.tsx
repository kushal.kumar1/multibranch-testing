import React from "react";
import styled from "styled-components";

const Input = styled.input`
    justify-content: space-between;
    width: 45px;
    height: 45px;
    margin: 5px;
    border-radius: 5px;
    text-align: center;
    font-size: 25px;
`;

const InputBox = ({
    type,
    handleKeyDown,
    handleChange,
    handleFocus,
    name,
    ...restProps
}) => {
    return (
        <Input
            type={type}
            onKeyDown={handleKeyDown}
            onChange={handleChange}
            onFocus={handleFocus}
            maxLength="1"
            name={name}
            className="input input-sm"
            {...restProps}
        />
    );
};

export default InputBox;
