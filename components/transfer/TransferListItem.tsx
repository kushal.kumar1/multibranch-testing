import moment from 'moment';
import Link from '../Link';
import Transfer from '../../types/transfer';

const ListItem = ({ item }: { item: Transfer }) =>{
  return (
    <Link href={`/transfer/${item.id}`}>
      <div className="card p-md ripple">
        <div className="grid gutter-between no-select">
          <div>
            <div className="grid m-center">
              <span className="col mr-sm">
                <i className="far fa-dot-circle orange"></i>
              </span>
              <div className="col">
                <span className="semi-bold md">#{item.id}</span>
                <div className="xs">
                  {moment.unix(item.created_at).format('Do MMM YY hh:mm A')}
                </div>
              </div>
            </div>
          </div>
          <div>
            <span className={`tag-${item.status} tag upper`}>
              {item.status}
            </span>
          </div>
        </div>
      </div>
    </Link>
  );
};

export default ListItem;
