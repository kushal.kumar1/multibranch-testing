import styled from "styled-components";

const Root = styled.div`
  position: fixed;
  width: 60px;
  height: 60px;
  bottom: 40px;
  right: 20px;
  background-color: var(--color-blue-dark);
  border-radius: 50px;
  box-shadow: 2px 2px 3px var(--color-grey);
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  color: var(--color-white);

  .extra {
    position: absolute;
    top: 0;
    left: 0;
    background: var(--color-red);
    height: 30%;
    width: 30%;
    text-align: center;
    border-radius: 50%;
  }

`;

interface Props {
    faClass?: string,
    onClick: any,
    label?: string,
    count?: number,
}

const FloatingButton = ({onClick, faClass, label, count}: Props) => {
    const className = faClass ?? "fa fa-plus";
    return (
        <Root onClick={onClick}>
            
            {count ? 
                <div className="extra xs">
                    {count}
                </div> : <></>
            }

            <i className={className}></i>
            {label && <span className="xs">{label}</span>}
        </Root>
    );
}

export default FloatingButton;