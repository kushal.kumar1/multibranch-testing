import { useState } from "react";
import styles from "./searchBar.module.css";

const SearchBar = ({ handleSearch, text,changeText }) => {

  const handleSubmit = (e) => {
    e.preventDefault();
    // do something with text
    handleSearch(text);
  };

  return (
    <form className={styles.searchBar}>
      <input
        type="text"
        value={text}
        placeholder="Search"
        onChange={(e) => changeText(e.target.value)}
        className="input"
      />
      <button type="submit" onClick={handleSubmit} className="btn btn-orange">
        <img src="/images/search.svg" alt="Search" />
      </button>
    </form>
  );
};

export default SearchBar;
