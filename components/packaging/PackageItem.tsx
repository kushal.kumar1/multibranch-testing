import { PackagingItemData } from "../../types/PackagingItem";
import Link from "../Link";

interface propType {
  item: PackagingItemData;
}

const PackageItem = ({ item }: propType) => {
  return (
    <Link
      href={{
        pathname: `/packaging/${item.order_id}`,
        query: { shipment_id: item.id },
      }}
    >
      <div className="col-12 mt-md">
        <div className="card p-md sm">
          <div className="grid gutter-sm">
            <div className="col-10">
              <div className="semi-bold md">ShipmentId :{item.id}</div>
              <div className="mt-xs xs">OrderId : {item.order_id}</div>
              <div className="mt-xs xs">No of items : {item.total_items}</div>
            </div>
            {
              <div className="col-2">
                <div style={{ border: "none" }} className="blue center mt-xs">
                  <i style={{ fontSize: "x-large" }} className="fas fa-chevron-right"></i>
                  <br />
                  <div className="xs">Start</div>
                </div>
              </div>
            }
          </div>
        </div>
      </div>
    </Link>
  );
};

export default PackageItem;
