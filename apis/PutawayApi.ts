import Api from "./Api";
import qs from "qs";

export default class PutawayApi extends Api {

    init = async () => {
        const url = this.getUrl("putaway/init");
        return await this.getClient().post(url);
    };

    searchProduct = async(term: number, type?: string)  => {
        const url = this.getUrl(`migrations/putaway/search/products?term=${term}${type ? `&type=cart` : ""}`);
        return await this.getClient().get(url);
    }

    searchStorage = async(value) => {
        const url = this.getUrl(`search/storages?term=${value}`);
        return await this.getClient().get(url);
    }

    assignStorage = async(params) => {
        const url = this.getUrl(`migrations/putaway/place-item`);
        return await this.getClient().post(url, qs.stringify(params));
    }

    loadItems = async() => {
        const url = this.getUrl("migrations/putaway/carts");
        return await this.getClient().get(url);
    }

    addItem = async(params) => {
        const url = this.getUrl(`putaway/item`);
        return await this.getClient().post(url, qs.stringify(params));
    }

    start = async() => {
        const url = this.getUrl("migrations/putaway/start");
        return await this.getClient().post(url);
    }

    addToCart =async (params) => {
        const url = this.getUrl(`migrations/putaway/carts/add-item`);
        return await this.getClient().post(url, qs.stringify(params)); 
    }

    // close = async(id: number) => {
    //     const url = this.getUrl("migrations/putaway/close");
    //     return await this.getClient().post(url);
    // }




}