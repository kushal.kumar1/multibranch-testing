import Api from "./Api";
import qs from "qs";

export default class PackagingAPI extends Api {

    //TODO change stubs 
    loadItems = async({per_page, page, searchedTerm}) => {

        let url = "";
        if(searchedTerm.trim() === "") {
            url = this.getUrl(`shipments?per_page=${per_page}&page=${page}`);
        }
        else {
            url = this.getUrl(`shipments?per_page=${per_page}&page=${page}&term=${searchedTerm}`);
        }
        //const items =  transferlist.filter(item=> item.id == id);
        //return  items[0];
        return await this.getClient().get(url);
    }

    updateShipment = async (params,id) => {
        const url = this.getUrl(`shipments/${id}/pack-shipment`);
        return await this.getClient().post(url, qs.stringify(params));
    }

    searchStorage = async(value) => {
        const url = this.getUrl(`search/beat-locations?term=${value}`);
        return await this.getClient().get(url);
    }


}
