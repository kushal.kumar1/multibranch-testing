import Api from "./Api";
import qs from "qs";

export default class DispatchApi extends Api {

    //TODO change stubs 
    loadLocations = async() => {
        const url = this.getUrl("beat-locations");
        //const items =  transferlist.filter(item=> item.id == id);
        //return  items[0];
        return await this.getClient().get(url);
    }

    loadItems = async(uri) => {
        const url = this.getUrl(uri);
        return await this.getClient().get(url);
    }

    markPickedItem =async (id : number) => {
        const url = this.getUrl(`shipments/${id}/load-shipment`);
        return await this.getClient().post(url); 
    }

}
