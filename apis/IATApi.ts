import Api from "./Api";
import qs from "qs";

export default class IATApi extends Api {
    callApi = async ({ id, params }) => {
        const url = this.getUrl(`picklists/irt/${id}/pick-item`);
        return await this.getClient().post(url, qs.stringify(params));
    };

    checkCorrectStorageLabel = async ({
        storageLabel,
        productId,
        quantity,
    }) => {
        const url = this.getUrl(
            `picklists/irt/check-item?quantity=${quantity}&product_id=${productId}&storage_label=${storageLabel}`
        );
        return await this.getClient().get(url);
    };
}
