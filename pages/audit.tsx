import {NextPage} from "next";
import Page from "../layouts/Page";
import AuditListItem from "../components/audit/AuditListItem";
import styled from "styled-components";
import FloatingButton from "../components/FloatingButton";
import Loader from "../components/Loader";
import EmptyMessage from "../components/EmptyMessage";
import useApi from "../hooks/useApi";
import {useRouter} from "next/router";

import Audit from "../types/Audit";

const Root = styled.div`
  .card:not(:first-child) {
    margin-top: var(--dimen-sm);
  }
`;

const AuditPage: NextPage = () => {
    const apiConfig = {path: 'audits'};
    const {results, loading} = useApi<Audit[]>(apiConfig); // TODO change to specidic type
    const router = useRouter();


    return (
        <Page title="AUDIT LIST">
            <Root>
                {loading && <Loader/>}
                {results && results.length > 0 && results.map((item) => <AuditListItem item={item} key={item.id}/>)}
                {results && results.length == 0 &&
                <EmptyMessage message="Nothing assigned for Audit yet."></EmptyMessage>}
            </Root>
        </Page>
    );
}

export default AuditPage;
