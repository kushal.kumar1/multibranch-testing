import {NextPage} from "next";
import Page from "../layouts/Page";
import ListItem from "../components/putaway/ListItem";
import styled from "styled-components";
import FloatingButton from "../components/FloatingButton";
import Loader from "../components/Loader";
import EmptyMessage from "../components/EmptyMessage";
import useApi from "../hooks/useApi";
import Putaway from "../types/Putaway";
import {useDispatch} from "react-redux";
import {useRouter} from "next/router";
import PutawayAction from "../redux/actions/PutawayAction";
import { PutawayItem } from "../types/PutawayItem";
import Link from "../components/Link";

const Root = styled.div`
  .card:not(:first-child) {
    margin-top: var(--dimen-sm);
  }
`;

const PutawayPage: NextPage = () => {
    const apiConfig = {path: 'migrations/putaway'};
    const {results, loading, setResults} = useApi<PutawayItem[]>(apiConfig);
    const router = useRouter();

    const dispatch = useDispatch();
  

    return (
        <Page title="PUTAWAY LIST">
            <Root>
                {loading && <Loader/>}
                {results && results.length > 0 && results.map((item) => <ListItem setResults={setResults} item={item} key={item.id}/>)}
                {results && results.length == 0 &&
                <EmptyMessage message="Keep Rolling. Hit the [+] button!"></EmptyMessage>}
            </Root>
            <Link href={`/putaway/cart`}>
            <FloatingButton 
                    count={0}  //getting items from API
                    label="CART"
                    faClass="fas fa-shopping-cart"
                    onClick={()=>{}}
                />
            </Link>
           
        </Page>
    );
}

export default PutawayPage;
