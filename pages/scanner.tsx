import {useState} from "react";
import Scanner from "../components/Scanner";
import styled from "styled-components";

const INFO = styled.div`
  position: absolute;
  bottom: 20px;
`;


const ScannerPage = () => {
    const [barcode, setBarcode] = useState(null);

    return (
        <>
            <Scanner onDetect={setBarcode}/>
            <INFO>barcode: {barcode}</INFO>
        </>
    );
}

ScannerPage.isPrivate = false;

// @ts-ignore
export default ScannerPage;
