import Link from "../components/Link";
import EmptyMessage from "../components/EmptyMessage";

export default function Custom404() {
    return (
        <div className="grid c-center m-center bg-white col grow">
            <EmptyMessage message="Looks like you are lost bro!">
                <Link href="/">
                    <button className="btn btn-lg btn-orange mt-md">Home</button>
                </Link>
            </EmptyMessage>
        </div>
    )
}
