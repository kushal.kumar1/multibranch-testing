import Page from "../../layouts/Page";
import Loader from "../../components/Loader";

import style from "./iatCard.module.css";

import Link from "next/link";

import useApi from "../../hooks/useApi";
const apiConfig = { path: "/picklists/irt" };

const IAT = () => {
    const { results: picklistItems, loading } = useApi<any[]>(apiConfig);

    return (
        <Page title="IAT" backUrl="/">
            {loading && <Loader />}
            <section className={style.iat}>
                {picklistItems && picklistItems?.length === 0 ? (
                    <div className={style.noItem}>No Items found</div>
                ) : (
                    <></>
                )}
                {picklistItems?.map((item) => (
                    <Link href={`/iat/${item.id}`} key={item.id}>
                        <div className={style.iatCard}>
                            <div className={style.left}>
                                <div className="md bold">
                                    Picklist ID # {item?.id}
                                </div>
                                <div className={style.product}>
                                    Products - {item?.meta?.items_count || "NA"}
                                </div>
                            </div>
                            <div
                                className={`${style.right} ${
                                    item.status === "picked"
                                        ? style.blue
                                        : style.green
                                } xs`}
                            >
                                {item.status === "picked" ? (
                                    <>
                                        <img
                                            src="/images/angleRight.svg"
                                            alt="arrow"
                                        />
                                        <div>Start</div>
                                    </>
                                ) : (
                                    <>
                                        <img
                                            src="/images/completed.svg"
                                            alt="completed"
                                        />
                                        <div>Completed</div>
                                    </>
                                )}
                            </div>
                        </div>
                    </Link>
                ))}
            </section>
        </Page>
    );
};

export default IAT;
