import { useState, useRef, useEffect } from "react";
import Page from "../../../../layouts/Page";

import { useRouter } from "next/router";

import style from "./iatProduct.module.css";

import { useDispatch } from "react-redux";
import AppAction from "../../../../redux/actions/AppAction";
import { ModalName } from "../../../../components/ModalManager";
import { scantype } from "../../../../types/IAT";

import QuantityHandle from "../../../../components/iat/QuantityHandle";
import { useThunkyDispatch } from "../../../../redux/store";
import IATAction from "../../../../redux/actions/IATAction";
import { AppActionTypes } from "../../../../redux/ActionTypes";
import Loader from "../../../../components/Loader";
import sleep from "../../../../utils/sleep";

const IATProduct = () => {
    const router = useRouter();
    const {
        iatId,
        productId,
        name,
        mrp,
        product_image,
        location,
        barcode,
        skip_reason,
        skipped_quantity,
        itemsCount,
        storage_id
    } = router.query;

    const skippedQuantity = +skipped_quantity;

    const dispatch = useDispatch();
    const dispatch1 = useThunkyDispatch();

    const [productData, setProductData] = useState([]);
    const locationRef = useRef(null);
    const [loading, setLoading] = useState(false);

    const openNoQuantityFoundModal = () => {
        dispatch(
            AppAction.switchModal(ModalName.IAT_CONFIRM, {
                action: handleNoQuantityFound,
                content: `Are you sure ? you did not find ${getRemainingQuantities(
                    skippedQuantity,
                    productData
                )} quantity of this product`,
            })
        );
    };

    const handleNoQuantityFound = () => {
        callApiForIAT();
    };

    const openScanModal = (type: scantype) => {
        dispatch(
            AppAction.switchModal(ModalName.IAT_SCAN, {
                productModal: type === "product" ? true : false,
                action: handleStorageId,
                productData: barcode,
            })
        );
    };

    const handleStorageId = async (storageLabel: string) => {
        locationRef.current = storageLabel;
        await sleep(2000);
        dispatch(
            AppAction.switchModal(ModalName.IAT_SCAN, {
                productModal: true,
                productBarcode: barcode,
                action: handleProductCheck,
            })
        );
    };

    const handleProductCheck = () => {
        const maxQuanityAllowed = getRemainingQuantities(
            skippedQuantity,
            productData
        );
        dispatch(
            AppAction.switchModal(ModalName.IAT_QUANTITY_SAVE, {
                // action: saveQuantityWithLocation,
                action: checkLocationWithQuantity,
                message: "How many items did you find ?",
                maxQuanityAllowed,
            })
        );
    };

    const checkLocationWithQuantity = async (quantity: number) => {
        // console.log("lable", locationRef.current);

        if(location === locationRef.current){
            saveQuantityWithLocation(quantity);
        }
        else{
            setLoading(true);
            const response = await dispatch1(
                IATAction.checkCorrectStorageLabel({
                    storageLabel: locationRef.current,
                    productId: productId as string,
                    quantity,
                })
            );
            setLoading(false);
            if (response) {
                saveQuantityWithLocation(quantity);
            }
        }
    };

    const saveQuantityWithLocation = (quantity: number) => {
        const temp = [...productData];
        temp.push({
            storage_label: locationRef.current,
            quantity: quantity,
        });

        setProductData(temp);
    };

    const callApiForIAT = async () => {
        const pna = getRemainingQuantities(skippedQuantity, productData);

        const sendData = {
            product_id: productId,
            items: productData,
            pna: pna,
            storage_id: storage_id
        };

        setLoading(true);
        const data = await dispatch1(
            IATAction.callApi({ id: iatId, params: sendData })
        );
        setLoading(false);

        if (data) {
            dispatch({
                type: AppActionTypes.DISPLAY_MESSAGE,
                payload: {
                    text: "Successfully Processed Item",
                    isError: false,
                    milliseconds: 1000,
                },
            });
            if (+itemsCount > 1) {
                router.back();
            } else {
                router.replace("/iat");
            }
        }
    };

    return (
        <Page title="PID">
            {loading && <Loader />}
            <section className={style.iat}>
                <div className={style.product}>
                    <div className={style.imageWrapper}>
                        <img src={product_image as string} alt="product" />
                    </div>

                    <div className={style.productInfo}>
                        <div className="bold sm">#PID - {productId}</div>
                        <div className="bold md">{name}</div>
                        <div className="bold sm">MRP - ₹{mrp}</div>
                        <div className="xs">Barcode - {barcode}</div>
                        <div className="xs">Location - {location}</div>
                        <div className="xs">Skipped - {skip_reason}</div>
                        <div className="xs">
                            Total QTY Required - {skippedQuantity}
                        </div>
                    </div>
                </div>
                {productData?.map((data, index) => (
                    <div className={style.card} key={index}>
                        <div className="bold md">
                            {data.quantity} Quantity found
                        </div>
                        <div className="xs">Location: {data.storage_label}</div>
                    </div>
                ))}

                {getRemainingQuantities(skippedQuantity, productData) === 0 ? (
                    <button
                        className={`btn w-full h-full ${style.no} mt-md`}
                        onClick={callApiForIAT}
                    >
                        Proceed
                    </button>
                ) : (
                    <QuantityHandle
                        openScanModal={openScanModal}
                        openNoQuantityFoundModal={openNoQuantityFoundModal}
                        productData={productData}
                    />
                )}
            </section>
        </Page>
    );
};

export default IATProduct;

const getRemainingQuantities = (
    skippedQuantity: number,
    productData: any[]
) => {
    if (productData.length === 0) {
        return skippedQuantity;
    }

    let foundQuantity = 0;
    for (let i = 0; i < productData.length; i++) {
        foundQuantity += productData[i].quantity;
    }

    return skippedQuantity - foundQuantity;
};
