import Page from "../../../layouts/Page";
import { useRouter } from "next/router";

import useApi from "../../../hooks/useApi";

import style from "../iatCard.module.css";
import Loader from "../../../components/Loader";

const IATPicklistItems = () => {
    const router = useRouter();
    const { iatId } = router.query;
    const apiConfig = { path: `/picklists/irt/${iatId}/items` };

    const { results: picklistItems, loading } = iatId
        ? useApi<any[]>(apiConfig)
        : { results: [], loading: "" };

    const handleClick = ({ pathname, index }) => {
        const data = picklistItems[index];
        router.push({
            pathname,
            query: {
                name: data?.name,
                mrp: data?.mrp,
                product_image: data?.product_image,
                location: data?.location,
                barcode: data?.barcode,
                skip_reason: data?.skip_reason,
                skipped_quantity: data?.skipped_quantity,
                itemsCount: picklistItems ? picklistItems?.length : 1,
                storage_id: data?.storage_id
            },
        });
    };

    return (
        <Page title="Picklist Items">
            <section className={style.iat}>
                {loading && <Loader />}
                {picklistItems && picklistItems?.length === 0 ? (
                    <div className={style.noItem}>No Items found</div>
                ) : (
                    <></>
                )}
                {picklistItems &&
                    picklistItems?.map((item, index) => (
                        <div
                            className={style.iatCard}
                            key={item.product_id}
                            onClick={() =>
                                handleClick({
                                    pathname: `/iat/${iatId}/${item.product_id}`,
                                    index,
                                })
                            }
                        >
                            <div
                                className={style.leftPicklistItems}
                                style={{ maxWidth: "75%" }}
                            >
                                <img
                                    src={item.product_image}
                                    alt={item.name}
                                    className="mr-sm"
                                    style={{ objectFit: "cover" }}
                                />
                                <div className={style.productInfo}>
                                    <div className="bold sm">#PID</div>
                                    <div className="bold md">{item.name}</div>
                                    <div className="bold xs">
                                        MRP - ₹{item.mrp}
                                    </div>
                                    <div className="xs">
                                        Location - {item.location}
                                    </div>
                                    <div className="xs">
                                        Skipped - {item.skip_reason}
                                    </div>
                                    <div className="xs">
                                        QTY Required - {item.skipped_quantity}
                                    </div>
                                </div>
                            </div>
                            <div className={`${style.right} ${style.blue} xs`}>
                                <img src="/images/angleRight.svg" alt="arrow" />
                                <div>Resolve</div>
                            </div>
                        </div>
                    ))}
            </section>
        </Page>
    );
};

export default IATPicklistItems;
