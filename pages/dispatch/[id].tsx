import { useEffect, useState } from "react";

import { useRouter } from "next/router";
import OrderItem from "../../components/dispatch/orderItem";
import Pie from "../../components/dispatch/Pie";
import Page from "../../layouts/Page";
import Loader from "../../components/Loader";

import { RootState, useThunkyDispatch } from "../../redux/store";
import DispatchAction from "../../redux/actions/DispatchAction";
import DispatchApi from "../../apis/DispatchApi";
import { useSelector } from "react-redux";
import Toolkit from "../../utils/Toolkit";

const DispatchDetailPage = () => {
    const router = useRouter();
    const { id, label } = router.query;

    const dispatch = useThunkyDispatch();
    const rootState = useSelector((state: RootState) => state);
    const apiProps = Toolkit.mapStateToApiProps(rootState);

    const [beatLocationData, setBeatLocationData] = useState<any>({});
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        getBeatLocationData();
    }, [id]);

    const getBeatLocationData = async () => {
        setLoading(true);
        try {
            if (id) {
                const uri = `beat-locations/${id}/shipments`;
                const response = await new DispatchApi(apiProps).loadItems(uri);
                const metaData = {
                    loaded : 0,
                    available : response?.data?.data?.length || 0,
                }
                const data = {
                    data: response.data.data,
                    metaData,
                }
                // setBeatLocationData(response?.data);
                setBeatLocationData(data);
            }
        } catch (error) {
            console.log("error: ", error);
        }
        setLoading(false);
    };

    const handleMarkItem = async (id: number) => {
        // now call post api and if success, then call get
        setLoading(true);
        const response = await dispatch(DispatchAction.markPickedItem(id));
        setLoading(false);
        if (response) {
            //means success, then change the status of that ID from the frontend
            const tempData = [...beatLocationData.data];
            const tempMeta = {...beatLocationData.metaData};
            //increaseing the loaded items
            if(tempMeta?.loaded < tempMeta?.available){
                tempMeta.loaded += 1;
            }
            const index = tempData.findIndex((item) => item.id === id);
            tempData[index].status = "dispatched";
            setBeatLocationData({
                data: tempData,
                metaData: tempMeta,
            });
        }
    };
    return (
        <Page title={label || ""}>
            {loading && <Loader />}
            <div className="gutter-sm mt-sm ">
                <div className="col-12 center">
                    {beatLocationData?.metaData?.available ===
                    beatLocationData?.metaData?.loaded ? (
                        <img
                            src="/images/allCompleted.svg"
                            alt="All Completed"
                            width="200"
                        />
                    ) : (
                        <Pie
                            totalItems={
                                // beatLocationData?.meta?.loaded +
                                beatLocationData?.metaData?.available || 0
                            }
                            completed={beatLocationData?.metaData?.loaded || 0}
                            colour="#1DBB9A"
                        />
                    )}
                </div>

                <OrderItem
                    items={beatLocationData.data}
                    handleMarkItem={handleMarkItem}
                />

                {beatLocationData?.metaData?.available ===
                    beatLocationData?.metaData?.loaded && (
                    <button className="btn btn-blue mt-lg w-full" onClick={()=>router.replace("/dispatch")}>
                        Completed
                    </button>
                )}
            </div>
        </Page>
    );
};

export default DispatchDetailPage;
