import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import DispatchApi from "../../apis/DispatchApi";
import DispatchItem from "../../components/dispatch/DispatchItem";
import Loader from "../../components/Loader";
import Page from "../../layouts/Page";
import { AppActionTypes } from "../../redux/ActionTypes";
import { RootState, useThunkyDispatch } from "../../redux/store";
import Toolkit from "../../utils/Toolkit";

const DispatchList = () => {
    const [loading, setLoading] = useState(true);
    const [results, setResults] = useState([]);
    const dispatch = useThunkyDispatch();

    useEffect(() => {
        loadLocation();
    }, []);

    const rootState = useSelector((state: RootState) => state);
    const apiProps = Toolkit.mapStateToApiProps(rootState);

    const loadLocation = async () => {
        setLoading(true);

        try {
            const response = await new DispatchApi(apiProps).loadLocations();
            setResults(response.data.data);
        } catch (error) {
            const err = error?.response?.data?.message || "Something went wrong";
            dispatch({
                type: AppActionTypes.DISPLAY_MESSAGE,
                payload: {text: err, isError: true, milliseconds: 2000}
              });
        }
        setLoading(false);
    };

    return (
        <Page title="DISPATCH" backUrl="/">
            <div className="grid gutter-sm mt-sm">
                {!loading ? (
                    results.map((item, index) => (
                        <DispatchItem item={item} key={item.id} />
                    ))
                ) : (
                    <Loader />
                )}
                {!loading && results.length === 0 && (
                    <div className="col-12 center font-lg bold">No Data Found</div>
                )}
            </div>
        </Page>
    );
};

export default DispatchList;
