import { NextPage } from "next";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import Page from "../layouts/Page";
import { AppActionTypes } from "../redux/ActionTypes";
import router from "next/router";
import AppAction from "../redux/actions/AppAction";
import { RootState } from "../redux/store";
const SelectDevicePage: NextPage = () => {
  const type = useSelector((state: RootState) => state.appState.deviceType);

  const [deviceType, setDeviceType] = useState<string | null>(type);
  const dispatch = useDispatch();

  useEffect(() => {
    if (deviceType) {
      setTimeout(() => {
        dispatch(AppAction.setDeviceType(deviceType));
        router.push("/");
      }, 500);
    }
  }, [deviceType]);

  return (
    <Page title="PLEASE SELECT DEVICE">
      <div className="grid flex gutter-around mt-xl">
        <div
          onClick={() => setDeviceType("mobile")}
          style={{
            ...imageContainer,
            background:
              deviceType === "mobile" ? "var(--color-orange-pop)" : "#c0b7b7",
          }}
          className="col-5 center mt-xl"
        >
          Modile phone
          <img
            src="/images/mobile-device.png"
            alt="mobile device"
            width="100"
          />
        </div>
        <div
          onClick={() => setDeviceType("scanner")}
          style={{
            ...imageContainer,
            background:
              deviceType === "scanner" ? "var(--color-orange-pop)" : "#c0b7b7",
          }}
          className="col-5 center mt-xl"
        >
          Scanner Device
          <img
            src="/images/scanner-device.png"
            alt="scanner device"
            width="100"
          />
        </div>
      </div>
    </Page>
  );
};

const imageContainer = {
  color: "white",
  fontWeignt: "bold",
};

export default SelectDevicePage;
