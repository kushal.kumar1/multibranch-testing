import Page from "../../../../layouts/Page";
import React, {useEffect, useState} from "react";
import AuditAction from "../../../../redux/actions/AuditAction";
import {GetServerSideProps} from "next";
import { AuditItem as AuditItemType, AuditItemStatus} from "../../../../types/AuditItem";
import {useThunkyDispatch} from "../../../../redux/store";
import EmptyMessage from "../../../../components/EmptyMessage";
import Loader from "../../../../components/Loader";
import styled from "styled-components"
import { AppActionTypes } from "../../../../redux/ActionTypes";
import AuditItem from "../../../../components/audit/AuditItem";
import AppAction from "../../../../redux/actions/AppAction";
import { ModalName } from "../../../../components/ModalManager";


const AuditDetailsPage = ({id,location,product,audit_name,label}) => {
    const [loading, setLoading] = useState(false);
    const [items, setItems] = useState<AuditItemType[] | []>([]);
    const [auditType, setAuditType] = useState('');
    const [storagelabel, setStoragelabel] = useState(null);
    const dispatch = useThunkyDispatch();

    const load = async (id: number) => {
        //const  query = router.query;
        setLoading(true);
        try {
            const data = await dispatch(AuditAction.loadItems(id,location!= null ? 'storageId':null,location !=null ?location:null))
            if (data != null){ 
                setItems(data);
                if (data.length > 0){
                    setStoragelabel(data[0].storage ? data[0].storage.label : null);
                }
                if (location != null) {
                    setAuditType('location')
                } else {
                    setAuditType('product');
                }
            }
            
        } catch (error) {
            dispatch({
                type: AppActionTypes.DISPLAY_MESSAGE,
                payload: {
                    text:
                        error?.response?.data?.message ||
                        "Something went wrong, please try again later",
                    isError: true,
                    milliseconds: 2000,
                },
            });
            
        }
        setLoading(false);
    };

    useEffect(() => {
        load(id);
    }, []);


    const addProdcutToStore = async (item) =>{
        dispatch({
            type: AppActionTypes.ADD_AUDIT_ITEM,
            payload: {currentAuditItem: item}
        });

    }

    const handleReportExtaItemModal = () => {
        dispatch(
            AppAction.switchModal(ModalName.EXTRA_ITEM_MODAL, {
                action: reportExtraItems,
                id: id,
                storageid:location,
                storagelabel:storagelabel,
                message: "Extra Items",
                auditType: auditType,
            })
        );
    };

    const reportExtraItems = (item) => {
        dispatch({
            type: AppActionTypes.LOAD_EXTRA_ITEM,
            payload: {extraItem: item}
        });
    };

    return (
        <Page title={<p>
            #{id} {audit_name}
            <br/>
            {auditType == "location" ? "Location Wise" : "Product Wise"} {label && `(${label})`}
        </p>}>
            <Root>
            <div className="grid gutter-sm c-center mt-md">
                <div className="col-9">
                    <div className="md bold mt-sm">ITEMS ({items.length})</div>
                </div>
                {/* { auditType =='location' && */}
                  <div className="col-3">
                  <button
                      className="btn btn-red-new btn-xs br-xs"
                      onClick={handleReportExtaItemModal}
                  >
                      Report Extra Item
                  </button>
              </div>
                {/* } */}

            </div>

            <div className="grid gutter-sm mt-sm">
                {!loading && items && items.length > 0 && items.filter((item:any)=> item.status == AuditItemStatus.ASSIGNED || 
                    item.status == AuditItemStatus.PENDING_APPROVAL).map(item =>
                    <AuditItem 
                        
                        item={item}
                        audittype={auditType} 
                        addProdcutToStore= {addProdcutToStore}
                        key={item.id} 
                        
                    />
                )}
                {!loading && items && items.filter((item:any)=> item.status == AuditItemStatus.ASSIGNED || 
                    item.status == AuditItemStatus.PENDING_APPROVAL).length == 0 && <EmptyMessage message="No items found!"/>}
            </div>
          
            {loading && <Loader/>}
           
            </Root>
        </Page>

    );
}

export const getServerSideProps: GetServerSideProps = async (context) => {
    const id = context.query.id;
    const location = context.query.location ? context.query.location : null;
    const product = context.query.product ? context.query.product : null;
    const audit_name = context.query.audit_name ? context.query.audit_name : null;
    const label = context.query.label ? context.query.label : null;
    return {
        props: {
            id,
            location,
            product,
            audit_name,
            label,
        },
    }
}

const Root = styled.div`
  padding-bottom: 100px;
`;
export default AuditDetailsPage;
