import Page from "../../layouts/Page";
import {useEffect, useState} from "react";
import PutawayAction from "../../redux/actions/PutawayAction";
import PutawayItem from "../../components/putaway/PutawayItem";
import {PutawayItem as PutawayItemType} from "../../types/PutawayItem";
import {useThunkyDispatch} from "../../redux/store";
import Storage from "../../types/Storage";
import EmptyMessage from "../../components/EmptyMessage";
import Loader from "../../components/Loader";
import { AppActionTypes } from "../../redux/ActionTypes";
import router from "next/router";

const PutawayDetailPage = ({}) => {
    const [loading, setLoading] = useState(false);
    const [items, setItems] = useState<PutawayItemType[] | []>([]);
    const [status, setStatus] = useState<Boolean>(null);
    const dispatch = useThunkyDispatch();

    const load = async () => {
        setLoading(true);
        const data = await dispatch(PutawayAction.load())
        if(data) {
            setItems(data.data);
            setStatus(data.putaway_started);
            // setId(data.id);
            setLoading(false);
        } else {
            setLoading(false)
        }
        
    };


    const start = async () => {
        const data = await dispatch(PutawayAction.start());
        if(data) setStatus(true);
    }

    

    const assignStorage = async (apiParams) => {
        setLoading(true)
        const res = await dispatch(PutawayAction.assignStorage(apiParams));
        if(res) {
            const storage: Storage = res.data.storage;
            const placed_quantity = res.data.placed_quantity;
            const item = items.find((item: PutawayItemType) => item.id === res.data.id);
            let updatedItems = items;
            if(res.excess_area_item){
                updatedItems =[...items, res.excess_area_item]
            }
            const updatedItem = {...item,placed_quantity, storage};
            setLoading(false);
            setItems([updatedItem, ...updatedItems.filter((item: PutawayItemType) => item.id != updatedItem.id)]);
            
            
        }
    }

    useEffect(()=> {
        if(!loading && items.length !== 0){
        const isMigrationComplete = items.filter((item:any)=>item.storage === null || item.storage === undefined).length == 0
            if(isMigrationComplete){
                dispatch({
                    type: AppActionTypes.DISPLAY_MESSAGE,
                    payload: {text: "PUTAWAY COMPLETE FOR CART ITEMS", isError: false, milliseconds: 3000}
                });
                router.push(`/putaway`);

            }
        }
    }, [items])

    useEffect(() => {
        load();
    }, []);

    const canStart = () => {
        return !status && items.length > 0;
    }


    


    // @ts-ignore
    const emptyStorageItems = items.filter(item => item.storage === null);

    return (
        <Page title={`CART`}>
            {
                !loading &&
                <div className="grid gutter-sm">
                    {/* <div className="col-6">
                        <button className="btn btn-sm btn-orange w-full" disabled={canAddItem() ? false : true}
                                onClick={() => dispatch(AppAction.switchModal(ModalName.ADD_PRODUCT, {onSubmit: addItem}))}>
                            <i className="fas fa-plus"></i> ADD ITEM
                        </button>
                    </div> */}
                    <div className="col-6">
                        {
                            status  &&
                            "In Progress"

                        }
                        {
                            emptyStorageItems.length >= 0 && !status  &&
                            <button className="btn btn-sm btn-yellow w-full" disabled={canStart() ? false : true}
                                    onClick={start}><i className="fas fa-play"></i> START PUTAWAY
                            </button>

                        }
                    </div>
                </div>
            }
            <div className="grid gutter-sm c-center mt-md">
                <div className="col-6">
                    <div className="md bold mt-sm">ITEMS ({items.length})</div>
                </div>
                <div className="col-6 right">
                    <span className={`tag-${status} tag upper`}>{status}</span>
                </div>
            </div>

            <div className="grid gutter-sm mt-sm">
                {!loading && items && items.length > 0 && items.map(item =>
                    <PutawayItem item={item} key={item.id} showStorageBtn={status}
                                 assignStorage={assignStorage}/>)
                }
                {!loading && items && items.length == 0 && <EmptyMessage message="No items found!"/>}
            </div>
            {loading && <Loader/>}
            {/* {
                canAddItem() && <FloatingButton faClass="fas fa-barcode" onClick={openScanner}/>
            } */}

        </Page>

    );
}


export default PutawayDetailPage;
