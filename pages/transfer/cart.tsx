const { loadavg } = require("os")
const { useEffect, useState } = require("react")
import router, { useRouter } from "next/router";
import EmptyMessage from "../../components/EmptyMessage";
import Loader from "../../components/Loader";
import CartItem from "../../components/transfer/CartItem";
import Page from "../../layouts/Page";
import TransferAction from "../../redux/actions/TransferAction";
import { useThunkyDispatch } from "../../redux/store";
import { AppActionTypes } from "../../redux/ActionTypes";

const CartPage = ({}) => {

    const [putawayStatus, setPutawayStatus] = useState(undefined);
    const [migrationItems,setMigrationItems] =useState([]);
    const [loading, setLoading] = useState(true);
    const dispatch  = useThunkyDispatch();
    
    const load = async () => {
        setLoading(true);
        const data = await dispatch(TransferAction.loadCart())
        setMigrationItems(data.data);
        setPutawayStatus(data.putaway_started);
       
        setLoading(false)
    };

    useEffect(() => {
          load();
    }, [])

    const startmigration = async () =>  {
       const startmigration = await dispatch(TransferAction.startMigration());
       if(startmigration){
        dispatch({
            type: AppActionTypes.DISPLAY_MESSAGE,
            payload: {text: 'migration started', isError: false, milliseconds: 2000}
        });
           setPutawayStatus(true);
       }
       
    }

    const putawayItem = async (apiParams) =>{
        setLoading(true);
        const putawayItem = await dispatch(TransferAction.putawayItem(apiParams));
        if (putawayItem) {  
         const itemIndex =  migrationItems.findIndex((item) => item.id === apiParams.putaway_item_id)

            if (apiParams.no_space ){
                // const  item =   putawayItem.
                 migrationItems[itemIndex] = putawayItem.data;
                 migrationItems.push(putawayItem.excess_area_item );
                 setMigrationItems([...migrationItems]);

            } else {
                migrationItems[itemIndex] = putawayItem.data;
                setMigrationItems([...migrationItems]);
            }
            const isMigrationComplete = migrationItems.filter(item=>item.placed_quantity === null || item.placed_quantity === undefined || item.placed_quantity == 0).length == 0
            if(isMigrationComplete){
                dispatch({
                    type: AppActionTypes.DISPLAY_MESSAGE,
                    payload: {text: "ALL ITEMS MIGRATED", isError: false, milliseconds: 3000}
                });
                router.push(`/transfer`);

            }
         //putawayItem = putawayItem.

        }
        setLoading(false)
    }
    return (
        <Page title={`CART`}>
            {!loading && migrationItems && migrationItems.length !== 0 ?
                <div className="grid gutter-sm">
                    <div className="col-6">
                    {!putawayStatus ?
                    <button className="btn btn-sm btn-orange w-full" onClick={startmigration} >
                            START MIGRATING
                        </button>
                        :
                        'In Progress'
                    }
                    </div>
                   
                    
                    {migrationItems.map((item, key) => (
                        <CartItem 
                        putawayStatus={putawayStatus}
                           migrationItems={migrationItems}
                            item={item} key={key}  
                        migrationstatus = {putawayStatus=='started'?true:false}
                        putawayItem = {putawayItem}
                        migrationId = {item.id}
                        />
                    ))}
                </div>
                :
                !loading && <EmptyMessage message="Cart is Empty" />
            }

            {
                loading && <Loader />
            }
        </Page>
    )
}

export default CartPage