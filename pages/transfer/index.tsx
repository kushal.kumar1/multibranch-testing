import Page from "../../layouts/Page";
import React, {useEffect, useState} from "react";
import PutawayAction from "../../redux/actions/PutawayAction";
import TransferAction from "../../redux/actions/TransferAction";
import {GetServerSideProps} from "next";
import AppAction from "../../redux/actions/AppAction";
import {ModalName} from "../../components/ModalManager";
import TransferItem from '../../components/transfer/TransferItem'
import {PutawayStatus} from "../../types/Putaway";
import { TransferItem as TransferItemType, TransferItemStatus} from "../../types/TransferItem";
import {useThunkyDispatch} from "../../redux/store";
import EmptyMessage from "../../components/EmptyMessage";
import Loader from "../../components/Loader";
import FloatingButton from "../../components/FloatingButton";
import { TransferStatus } from "../../types/transfer";
import Link from 'next/link';
import styled from "styled-components"
import useApi from "../../hooks/useApi";
import { AppActionTypes } from "../../redux/ActionTypes";


const TransferDetailPage = ({}) => {
    const [loading, setLoading] = useState(false);
    const [items, setItems] = useState<TransferItemType[] | []>([]);
    const [status, setStatus] = useState<string>(null);
    const [cartItems,setCartItems] = useState<TransferItemType[] | []>([]); 
    const [remarks, setRemarks] = useState([]);
    const [addedToCart, setAddedToCard] = useState(false)
    const dispatch = useThunkyDispatch();

    const load = async () => {
        setLoading(true);
        const data = await dispatch(TransferAction.load())
        if (data != null){  
        setItems(data.data);
        setStatus(data.status);
        setRemarks(data.from_remarks)
        setAddedToCard(data.added_to_cart);
        setCartItems(data.data.filter(item => item.cart_quantity > 0 && item.status != "completed"));
        }
        setLoading(false);
    };

    useEffect(() => {
        load();
    }, []);


    const canAddItemToCart = () => {
        return addedToCart;
    }


    const addToCart = async (apiParams) => {
        setLoading(true)
        const addedtoCart = await dispatch(TransferAction.addItem(apiParams.product, apiParams.quantity, apiParams.source_location, apiParams.remarks, apiParams.migration_item_id, apiParams.sku));
        if(addedtoCart) {
            const itemIndex = items.findIndex((item: TransferItemType) => item.product.id === apiParams.product);
            const item= items.find((item: TransferItemType) => item.product.id === apiParams.product && item.source_storage.id === apiParams.source_location);
            const updatedItem = {...item, cart_quantity: apiParams.quantity};
            items[itemIndex] = updatedItem;
            setCartItems([...cartItems, updatedItem]);
            setItems([...items]);
            dispatch({
                type: AppActionTypes.DISPLAY_MESSAGE,
                payload: {text: 'Successfully added item to cart', isError: false, milliseconds: 1000}
            });
        }
        setLoading(false)
        
    } 

    return (
        <Page title={`#TRANSFER LIST`}>
            <Root>
            <div className="grid gutter-sm c-center mt-md">
                <div className="col-6">
                    <div className="md bold mt-sm">ITEMS ({items.length})</div>
                </div>
                <div className="col-6 right">
                    <span className={`tag-${status} tag upper`}>{status}</span>
                </div>
            </div>

            <div className="grid gutter-sm mt-sm">
                {!loading && items && items.length > 0 && items.map(item =>
                    <TransferItem 
                        addToCart={addToCart} 
                        item={item} 
                        key={item.id} 
                        showStorageBtn={true}
                        migrationId ={item.id}
                        remarks={remarks}
                    />
                )}
                {!loading && items && items.length == 0 && <EmptyMessage message="No items found!"/>}
            </div>
          
            {loading && <Loader/>}
            {
                canAddItemToCart() && 
                <Link href={`/transfer/cart`} >
                <FloatingButton 
                    count={cartItems && cartItems.length}  //getting items from API
                    label="CART"
                    faClass="fas fa-shopping-cart"
                    onClick={()=>{}}
                />
                </Link>
            }
            </Root>
        </Page>

    );
}

const Root = styled.div`
  padding-bottom: 100px;
`;
export default TransferDetailPage;
