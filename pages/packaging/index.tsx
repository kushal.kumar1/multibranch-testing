import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import PackagingAPI from "../../apis/PackagingApi";
import Loader from "../../components/Loader";
import PackageItem from "../../components/packaging/PackageItem";
import Page from "../../layouts/Page";
import { AppActionTypes } from "../../redux/ActionTypes";
import { RootState, useThunkyDispatch } from "../../redux/store";
import { PackagingItemType } from "../../types/PackagingItem";
import Toolkit from "../../utils/Toolkit";
import SearchBar from "../../components/SearchBar";
import Pagination from "../../components/Pagination";

const per_page = 20;

const PackagingList = () => {
  const [loading, setLoading] = useState<Boolean>(true);
  const [results, setResults] = useState<PackagingItemType>();
  const [currentPage, setCurrentPage] = useState(1);
  const [text, setText] = useState("");
  const [isSearching, setIsSearching] = useState(false);

  useEffect(() => {
    if (isSearching && text.trim() === "") {
      load({ currentPage: 1, searchedTerm: "" });
      setIsSearching(false);
      setCurrentPage(1);
    }
  }, [text]);

  const dispatch = useThunkyDispatch();

  useEffect(() => {
    load({ currentPage: 1, searchedTerm: "" });
  }, []);

  const rootState = useSelector((state: RootState) => state);
  const apiProps = Toolkit.mapStateToApiProps(rootState);

  const load = async ({ currentPage, searchedTerm }) => {
    setLoading(true);

    try {
      const response = await new PackagingAPI(apiProps).loadItems({
        per_page,
        page: currentPage,
        searchedTerm,
      });
      setResults(response.data);
    } catch (error) {
      const err = error?.response?.data?.message || "Something went wrong";
      dispatch({
        type: AppActionTypes.DISPLAY_MESSAGE,
        payload: { text: err, isError: true, milliseconds: 2000 },
      });
    }
    setLoading(false);
  };

  const handleSearch = (term: string) => {
    if (!isSearching) {
      setIsSearching(true);
    }
    load({ currentPage: 1, searchedTerm: term });
    setCurrentPage(1);
  };

  return (
    <Page title="PACKAGING" backUrl="/">
      <div className="gutter-sm mt-sm">
        {!loading ? (
          <>
            <SearchBar
              handleSearch={handleSearch}
              text={text}
              changeText={(txt: string) => setText(txt)}
            />
            {results?.data?.map((item) => (
              <PackageItem item={item} key={item.id} />
            ))}
            <Pagination
              currentPage={currentPage}
              totalCount={results?.meta?.total || 0}
              pageSize={per_page}
              onPageChange={(page: number) => {
                setCurrentPage(page);
                load({ currentPage: page, searchedTerm: text });
              }}
            />
          </>
        ) : (
          <Loader />
        )}
        {!loading && results?.data?.length === 0 && (
          <div className="col-12 center font-lg bold">No Data Found</div>
        )}
      </div>
    </Page>
  );
};
export default PackagingList;
