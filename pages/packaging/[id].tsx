import router from "next/router";
import { useState } from "react";
import Page from "../../layouts/Page";
import AsyncSelect from "react-select/async";
// import PutawayApi from "../../apis/PutawayApi";
import { useSelector } from "react-redux";
import { RootState, useThunkyDispatch } from "../../redux/store";
import Toolkit from "../../utils/Toolkit";
import PackagingAPI from "../../apis/PackagingApi";
import { AppActionTypes } from "../../redux/ActionTypes";
import Scanner from "../../components/Scanner";
import { components } from "react-select";

const NoOptionsMessage = (props) => {
  return (
    <components.NoOptionsMessage {...props}>
      <span>No Products Found</span>
    </components.NoOptionsMessage>
  );
};
const PakageDetailPage = ({}) => {
  const [crates, setCrates] = useState<number>();
  const [boxes, setBoxes] = useState<number>();
  const [onFirstStep, setOnFirstStep] = useState<boolean>(true);
  const [selectedStorage, setSelectedStorage] = useState(null);
  const [scannerType, setScannerType] = useState(null);
  const [storageBarcode, setStorageBarcode] = useState(undefined);
  const [submiting, setSubmiting] = useState(false);
  const dispatch = useThunkyDispatch();

  const submitBoxesUsed = (e) => {
    e.preventDefault();
    setOnFirstStep(false);
    console.log("crates", crates, " boxes", boxes);
  };

  const onDetect = (barcode: { barcode: string }) => {
    if (scannerType == "storage") {
      setStorageBarcode(barcode);
    }
    setScannerType(null);
  };


  const onFinalSubmit = async (e) => {
    setSubmiting(true);
    e.preventDefault();
    console.log("boxes", boxes);
    console.log("crates", crates);
    console.log("selectedStorage", selectedStorage);
    const params = {
      storage_id: selectedStorage,
      crates: crates,
      boxes: boxes,
    };
    await new PackagingAPI(apiProps)
      .updateShipment(params, router.query.shipment_id)
      .then((res: any) => {
        setSubmiting(false);
        dispatch({
          type: AppActionTypes.DISPLAY_MESSAGE,
          payload: {
            text: res.data.message,
            isError: false,
            milliseconds: 2000,
          },
        });
        router.push(`/packaging`);
      })
      .catch((err) => {
        setSubmiting(false);
        console.log(err.response.data);
        if (err.response.data) {
          dispatch({
            type: AppActionTypes.DISPLAY_MESSAGE,
            payload: {
              text: err.response.data.message,
              isError: true,
              milliseconds: 2000,
            },
          });
        }
      });
  };

  const rootState = useSelector((state: RootState) => state);
  const apiProps = Toolkit.mapStateToApiProps(rootState);

  const loadStorages = async (value) => {
    const val = value || storageBarcode;
    const response = await new PackagingAPI(apiProps).searchStorage(val);
    return response.data.data;
  };

  return (
    <Page title={router.query.id}>
      {onFirstStep ? (
        <form className="mt-lg" onSubmit={submitBoxesUsed}>
          <div className="center">
            <h2 className="bold">No of boxes used</h2>
          </div>
          <div className="mt-md">
            <label className="label">total no. of boxes used</label>
            <input
              value={boxes}
              min={0}
              className="input input-sm"
              type="number"
              onChange={(e) => setBoxes(parseInt(e.target.value))}
            />
          </div>
          <div>
            <label className="label">total no. of crates used</label>
            <input
              value={crates}
              min={0}
              className="input input-sm"
              type="number"
              onChange={(e) => setCrates(parseInt(e.target.value))}
            />
          </div>
          <div className="mt-lg center">
            <button
              disabled={!(boxes >= 0 && crates >= 0)}
              className="btn btn-blue"
            >
              NEXT
            </button>
          </div>
        </form>
      ) : (
        <form className="mt-lg" onSubmit={onFinalSubmit}>
          <div className="center">
            <h2 className="bold">Beat Location</h2>
          </div>
          <label className="label">Assign to Storage</label>
          <div className="grid gutter-md c-center">
            <div className="col-10">
              <AsyncSelect
                autoFocus
                key={storageBarcode}
                components={{ NoOptionsMessage }}
                getOptionLabel={(e) => `${e.label}`}
                getOptionValue={(e) => e.id}
                loadOptions={loadStorages}
                onChange={(value: any) => {
                  setSelectedStorage(value.id);
                }}
                defaultInputValue={storageBarcode}
                defaultOptions={!!storageBarcode}
                defaultMenuIsOpen={!!storageBarcode}
              />
            </div>
            <div className="col-2 center">
              <button
                style={scannerBtnStyle}
                className="btn btn-orange"
                type="button"
                onClick={() => setScannerType("storage")}
              >
                <span className="fas fa-qrcode" />
              </button>
            </div>
          </div>
          <div className="mt-lg center">
            <button disabled={!selectedStorage || submiting} className="btn btn-blue">
              {submiting ? "SUBMITTING..." : "SUBMIT"}
            </button>
          </div>
        </form>
      )}
      {scannerType && (
        <>
          <div className={`full modal`} style={{ zIndex: 100 }}>
            <div className="close" onClick={() => setScannerType(null)}>
              <i className="fas fa-times"></i>
            </div>
            <Scanner onDetect={onDetect} />
          </div>
        </>
      )}
    </Page>
  );
};

const scannerBtnStyle = {
  display: "flex",
  justifyContent: "center",
  height: "2.5em",
  width: "2.5em",
};

export default PakageDetailPage;
