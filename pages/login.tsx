import React, {FormEvent, useEffect, useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import AppActions from '../redux/actions/AppAction';
import {RootState} from '../redux/store';
import Router from 'next/router';

const LoginPage = () => {
    const token = useSelector((state: RootState) => state.appState.token);
    const message = useSelector((state: RootState) => state.appState.message);
    const dispatch = useDispatch();
    const [mobile, setMobile] = useState('');
    const [otp, setOtp] = useState('');
    const [otpRequested, setOtpRequested] = useState(false);
    const [otpSubmitted, setOtpSubmitted] = useState(false);
    const [showMessage, setShowMessage] = useState(false);
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        if (token) {
            Router.push('/');
        }
        if (message){
            setLoading(false);
        }
        if (message && message.isError && otpRequested) {
            setShowMessage(true);
            if(!otpSubmitted){
                setOtpRequested(false);
            }
            else{
                setOtpSubmitted(false);
            }
        }
    }, [token, message]);

    const submit = async (e: FormEvent) => {
        e.preventDefault();
        setShowMessage(false);
        setLoading(true);
        if(!otpRequested){
            setOtpRequested(true);
            dispatch(AppActions.sendOtp({ mobile }));
        }
        else{
            setOtpSubmitted(true);
            dispatch(AppActions.login({ mobile, otp }));
        }
    }

    const resetOtpLogin = () => {
        setMobile('');
        setOtp('');
        setOtpRequested(false);
        setOtpSubmitted(false);
        setShowMessage(false);
    }

    return (
        <div className="grid c-center m-center h-full col grow bg-white">
            <div className="col-12 col-sm-6 col-lg-4">
                <div className="bg-white" style={{ 'padding': '20px 20px' }}>
                    <div className="xl center bold"><img src="/images/logo.png" width="100px"/></div>
                    <h1 className="md center mt-xl semi-bold">LOGIN</h1>
                    <form onSubmit={submit}>
                        <div className="input-group mt-lg">
                            <div className="input-group-prepend">
                                <span className="input-group-text">+91</span>
                            </div>
                            <input type="number" name="mobile" value={mobile} placeholder="Enter mobile number"
                                className="input" onChange={(e) => setMobile(e.target.value)} disabled={otpRequested} />
                            </div>
                        {
                            otpRequested && (
                                <div className="mt-md">
                                    <input type="password" name="otp" value={otp} placeholder="Enter OTP"
                                        className="input" onChange={(e) => setOtp(e.target.value)} />
                                </div>
                            )
                        }
                        {loading ?
                            <div className="xs center h-full">
                                <i className="fas fa-spinner fa-spin mr-xs"></i>
                            </div>
                            :
                            <button type="submit" className="btn btn-orange mt-lg w-full">{ otpRequested ? "LOGIN" : "REQUEST OTP" }</button>
                        }
                        {
                            otpRequested && (
                                <button type="button" className="btn btn-sm btn-default mt-lg w-full" onClick={resetOtpLogin}>Use Different Mobile Number</button>
                            )
                        }
                    </form>
                    <br></br>
                    { message && showMessage && (
                        <div className={ "alert" + (message.isError ? " alert-danger" : " alert-primary")} role="alert">
                            { message.text }
                        </div>
                    )}
                    <div className="mt-xl xs muted center">&copy; Odicea Distribution Technologies Pvt. Ltd. </div>
                </div>
            </div>
        </div>
    );
}

LoginPage.isPrivate = false;

export default LoginPage;
