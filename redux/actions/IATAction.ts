import { ThunkyAction } from "../store";
import Toolkit from "../../utils/Toolkit";
import { AppActionTypes } from "../ActionTypes";
import IATApi from "../../apis/IATApi";

const callApi = ({ id, params }): ThunkyAction<Promise<any>> => {
    return async (dispatch, getState) => {
        const apiProps = Toolkit.mapStateToApiProps(getState());

        try {
            const response = await new IATApi(apiProps).callApi({
                id,
                params,
            });

            if (response.data.error) {
                throw response.data.error;
            }
            return (
                // response?.data?.data ||
                // response?.data?.message ||
                "Successfully Processed Item"
            );
        } catch (e) {
            const errorMessage =
                e?.response?.data?.error ||
                e?.response?.data?.message ||
                "Something went wrong, please try again later";
            dispatch({
                type: AppActionTypes.DISPLAY_MESSAGE,
                payload: {
                    text: errorMessage,
                    isError: true,
                    milliseconds: 2500,
                },
            });
        }
    };
};

const checkCorrectStorageLabel = ({
    storageLabel,
    productId,
    quantity,
}: {
    storageLabel: string;
    productId: string;
    quantity: number;
}): ThunkyAction<Promise<any>> => {
    return async (dispatch, getState) => {
        const apiProps = Toolkit.mapStateToApiProps(getState());

        try {
            const response = await new IATApi(
                apiProps
            ).checkCorrectStorageLabel({
                storageLabel,
                productId,
                quantity,
            });

            if (response.data.error) {
                throw response.data.error;
            }
            return (
                response?.data?.data ||
                response?.data?.message ||
                "Successfully done"
            );
        } catch (e) {
            const errorMessage =
                e?.response?.data?.error ||
                e?.response?.data?.message ||
                "Something went wrong, please try again later";
            dispatch({
                type: AppActionTypes.DISPLAY_MESSAGE,
                payload: {
                    text: errorMessage,
                    isError: true,
                    milliseconds: 2500,
                },
            });
        }
    };
};

export default {
    callApi,
    checkCorrectStorageLabel,
};
