import {ThunkyAction} from "../store";
import PutawayApi from "../../apis/PutawayApi";
import Toolkit from "../../utils/Toolkit";
import {AppActionTypes} from "../ActionTypes";
import Putaway from "../../types/Putaway";
import {PutawayItem} from "../../types/PutawayItem";

const create = (): ThunkyAction<Promise<number>> => {
    return async (dispatch, getState) => {
        const apiProps = Toolkit.mapStateToApiProps(getState());
        const response = await new PutawayApi(apiProps).init();
        dispatch({
            type: AppActionTypes.DISPLAY_MESSAGE,
            payload: {text: "New putaway list created", isError: false, milliseconds: 2000}
        });
        return response.data.data.id;
    }
}

const addToCart = (apiParams): ThunkyAction<Promise<any>> => {
    return async (dispatch, getState) => {
        const apiProps = Toolkit.mapStateToApiProps(getState());
        
        try {
            const response = await new PutawayApi(apiProps).addToCart(apiParams);

            if(response.data.error){
                throw response.data.error
            }
            return response.data.data;
        }
        catch(e) {
            dispatch({
                type: AppActionTypes.DISPLAY_MESSAGE,
                payload: {text: e.response ? e.response.data.error : e, isError: true, milliseconds: 2000}
            });
        }
    }
}

const start = (): ThunkyAction<Promise<any>> => {
    return async (dispatch, getState) => {
        const apiProps = Toolkit.mapStateToApiProps(getState());
        
        try {
            const response = await new PutawayApi(apiProps).start();
            return response.data;
        }
        catch(e) {
            dispatch({
                type: AppActionTypes.DISPLAY_MESSAGE,
                payload: {text: e.response.data.message, isError: true, milliseconds: 2000}
            });
        }
    }
}

// const close = (id): ThunkyAction<Promise<any>> => {
//     return async (dispatch, getState) => {
//         const apiProps = Toolkit.mapStateToApiProps(getState());
//         try {
//             const response = await new PutawayApi(apiProps).close(id);
//             return response.data.data;
//         }
//         catch(e) {
//             dispatch({
//                 type: AppActionTypes.DISPLAY_MESSAGE,
//                 payload: {text: e.response.data.message, isError: true, milliseconds: 2000}
//             });
//         }
//     }
// }

const addItem = (id: number, productId: number, quantity: number): ThunkyAction<Promise<PutawayItem>> => {
    return async (dispatch, getState) => {
        const apiParams = {
            id: id,
            product_id: productId,
            quantity: quantity
        };
        const apiProps = Toolkit.mapStateToApiProps(getState());
        try {
            const response = await new PutawayApi(apiProps).addItem(apiParams);
            return response.data.data;
        }
        catch(e) {
            dispatch({
                type: AppActionTypes.DISPLAY_MESSAGE,
                payload: {text: e.response.data.message, isError: true, milliseconds: 2000}
            });
        }

    }
}

const assignStorage = (params): ThunkyAction<Promise<any>> => {
    return async (dispatch, getState) => {
        const apiProps = Toolkit.mapStateToApiProps(getState());
        try {
            const response = await new PutawayApi(apiProps).assignStorage(params);
            return response.data;
        }
        catch(e) {
            dispatch({
                type: AppActionTypes.DISPLAY_MESSAGE,
                payload: {text: e.response.data.message, isError: true, milliseconds: 2000}
            });
        }
    }
}

const load = (): ThunkyAction<Promise<{data:PutawayItem[], putaway_started: Boolean}>> => {
    return async (dispatch, getState) => {
        const apiProps = Toolkit.mapStateToApiProps(getState());
        
        try {
            const response = await new PutawayApi(apiProps).loadItems();
            return response.data;
        }
        catch(e) {
            dispatch({
                type: AppActionTypes.DISPLAY_MESSAGE,
                payload: {text: e.response.data.message, isError: true, milliseconds: 2000}
            });
        }
    }
}

export default {
    create, start, addItem, assignStorage, load, addToCart
}

