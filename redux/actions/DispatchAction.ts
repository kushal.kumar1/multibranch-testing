import {ThunkyAction} from "../store";
import PutawayApi from "../../apis/PutawayApi";
import Toolkit from "../../utils/Toolkit";
import {AppActionTypes} from "../ActionTypes";
import DispatchApi from "../../apis/DispatchApi";

const markPickedItem = (id: number): ThunkyAction<Promise<any>> => {
  return async (dispatch, getState) => {
      const apiProps = Toolkit.mapStateToApiProps(getState());
      try {
          const response = await new DispatchApi(apiProps).markPickedItem(id);
          return response.data.data || response?.data?.message || "Successfully Done";
      }
      catch(e) {
          dispatch({
              type: AppActionTypes.DISPLAY_MESSAGE,
              payload: {text: e.response.data.message, isError: true, milliseconds: 2000}
          });
      }

  }
}

export default {
  markPickedItem
}

