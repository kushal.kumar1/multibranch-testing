import {ThunkyAction} from "../store";
import TransferAPI from "../../apis/TransferAPI";
import Toolkit from "../../utils/Toolkit";
import {AppActionTypes} from "../ActionTypes";
import {} from "../../types/PutawayItem";
import { TransferItem } from "../../types/TransferItem";


const addItem = (productId: number, quantity: number, storage_location:number, remarks:string, migration_item_id: number, sku: string): ThunkyAction<Promise<any>> => {
    return async (dispatch, getState) => {
        const apiParams = {
            product_id: productId,
            source_storage_id:storage_location,
            quantity: quantity,
            remarks: remarks,
            migration_item_id: migration_item_id,
            sku: sku
        };
        const apiProps = Toolkit.mapStateToApiProps(getState());
        try {
            const response = await new TransferAPI(apiProps).addItemToCart(apiParams);
            if(response.data.error){
                throw response.data.error
            }
            return response.data.data;
        }
        catch(e) {
            dispatch({
                type: AppActionTypes.DISPLAY_MESSAGE,
                payload: {text: e.response ? e.response.data.message : e, isError: true, milliseconds: 2000}
            });
        }

    }
}

//TO DO changes
const load = (): ThunkyAction<Promise<any>> => {
    return async (dispatch, getState) => {
        const apiProps = Toolkit.mapStateToApiProps(getState());
        const response = await new TransferAPI(apiProps).loadItems();
        //return response;
        dispatch({
            type: AppActionTypes.LOAD_TRANSFERLIST,
            payload: {transferlist: response.data.data}
        });
        return response.data;
    }
}

const loadCart = (): ThunkyAction<Promise<any>> => {
    return async (dispatch, getState) => {
        const apiProps = Toolkit.mapStateToApiProps(getState());
        const response = await new TransferAPI(apiProps).loadCart();
        //return response;
        return response.data;
    }
}

const startMigration = (): ThunkyAction<Promise<any>> => {
    return async (dispatch, getState) => {
        const apiProps = Toolkit.mapStateToApiProps(getState());
        
        try {
            const response = await new TransferAPI(apiProps).startMigration();
            if(response.data.error){
                throw response.data.error
            } else {
                dispatch({
                    type: AppActionTypes.START_MIGRATION,
                    payload: {putawaystatus: true}
                });
                return response.data;
            }
            
        }
        catch(e) {
            dispatch({
                type: AppActionTypes.DISPLAY_MESSAGE,
                payload: {text: e.response ? e.response.data.message : e, isError: true, milliseconds: 2000}
            });
        }
    }
}

const putawayItem = (apiParams): ThunkyAction<Promise<any>> => {
    return async (dispatch, getState) => {
        const apiProps = Toolkit.mapStateToApiProps(getState());
       try{
        const response = await new TransferAPI(apiProps).migrateItem(apiParams);
        if(response.data.error){
            throw response.data.error
        }
        //return response;
        dispatch({
            type: AppActionTypes.MIGRATE_ITEM_SUCCESS,
            payload: {putawayItemMessage: response.data}
        });
        return response.data;
    } catch (err) {
        dispatch({
            type: AppActionTypes.DISPLAY_MESSAGE,
            payload: {text: err.response ? err.response.data.message : err, isError: true, milliseconds: 2000}
        });

    }
    }
}

// const closeMigration = (id): ThunkyAction<Promise<any>> => {
//     return async (dispatch, getState) => {
//         const apiProps = Toolkit.mapStateToApiProps(getState());
//         const response = await new TransferAPI(apiProps).closeMigration(id);
//         dispatch({
//             type: AppActionTypes.START_MIGRATION,
//             payload: {putawaystatus: 'closed'}
//         });
//         return response.data;
//     }
// }
export default {
     addItem, load, startMigration, putawayItem, loadCart
}

